-- phpMyAdmin SQL Dump
-- version 5.2.0-2.fc37
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Feb 14, 2023 alle 18:57
-- Versione del server: 10.5.18-MariaDB
-- Versione PHP: 8.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pcshop1`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `articolo`
--

CREATE TABLE `articolo` (
  `idA` int(11) NOT NULL,
  `Tipo` char(30) NOT NULL,
  `Produttore` char(30) NOT NULL,
  `Nome_articolo` varchar(255) NOT NULL,
  `Descrizione` varchar(4095) NOT NULL,
  `Immagine` char(30) NOT NULL,
  `Consumo` int(11) DEFAULT NULL,
  `Disponibilita` int(11) NOT NULL,
  `Prezzo` int(11) NOT NULL,
  `Socket_CPU` char(10) DEFAULT NULL,
  `Ram_DDR` int(2) DEFAULT NULL,
  `Formato_MB` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `articolo`
--

INSERT INTO `articolo` (`idA`, `Tipo`, `Produttore`, `Nome_articolo`, `Descrizione`, `Immagine`, `Consumo`, `Disponibilita`, `Prezzo`, `Socket_CPU`, `Ram_DDR`, `Formato_MB`) VALUES
(1, 'CPU', 'intel', 'I9 13900ks', '', '', 150, 23, 40, 'LGA1700', 5, NULL),
(2, 'CPU', 'intel', 'i9 11900T', '', '', 35, 10, 35, 'LGA1700', 5, NULL),
(3, 'CPU', 'intel', 'i5 12500T', '', '', 35, 67, 35, 'LGA1700', 5, NULL),
(4, 'CPU', 'Intel', 'I9 12900', '', '', 65, 6, 68, 'LGA1200', 4, NULL),
(5, 'CPU', 'intel', 'I9 11900', '', '', 65, 7, 45, 'LGA1200', 4, NULL),
(6, 'CPU', 'Intel', 'I5 12400G', '', '', 35, 2, 35, 'LGA', 4, NULL),
(7, 'CPU', 'AMD', 'Ryzen 5800 3DX', '', '', 150, 10, 120, 'AM4', 4, NULL),
(8, 'CPU', 'AMD', 'Ryzen 5900', '', '', 35, 11, 80, 'AM4', 4, NULL),
(9, 'CPU', 'AMD', 'Ryzen 7900 X', '', '', 65, 43, 90, 'AM5', 5, NULL),
(10, 'CPU', 'AMD', 'Ryzen 770', '', '', 80, 10, 88, 'AM5', 5, NULL),
(11, 'RAM', 'Corsair', 'CMK16GX4M2A2400C14R Vengeance LPX 16GB', '', '', NULL, 45, 60, NULL, 4, NULL),
(12, 'RAM', 'Corsair', 'Vengeance RGB PRO Kit da 16 GB', '', '', NULL, 4, 80, NULL, 4, NULL),
(13, 'RAM', 'Corsair', 'CMK16GX4M2A2400C14R Vengeance LPX 16GB', '', '', NULL, 45, 60, NULL, 4, NULL),
(14, 'RAM', 'Corsair', 'Vengeance RGB PRO Kit da 16 GB', '', '', NULL, 4, 80, NULL, 4, NULL),
(15, 'RAM', 'Corsair', 'Vengeance RGB PRO Kit da 32 GB', '', '', NULL, 12, 112, NULL, 5, NULL),
(16, 'RAM', 'Patriot', 'Memory Viper Steel RGB Kit da 32GB', '', '', NULL, 19, 130, NULL, 5, NULL),
(17, 'RAM', 'Patriot', 'Viper Blackout Kit da 32GB', '', '', NULL, 2, 184, NULL, 4, NULL),
(18, 'RAM', 'Kingston', 'HyperX Fury Blue Series 4–8GB', '', '', NULL, 44, 66, NULL, 3, NULL),
(19, 'RAM', 'Kingston', 'Predator HX436C18PB3AK4/128 Kit da 128GB', '', '', NULL, 4, 955, NULL, 4, NULL),
(20, 'RAM', 'Kingston', 'Predator HX432C16PB3K4/64 Kit da 64GB', '', '', NULL, 10, 122, NULL, 4, NULL),
(21, 'RAM', 'Kingston', 'Impact HX424S14IB2K2/16', '', '', NULL, 20, 115, NULL, 4, NULL),
(22, 'RAM', 'Crucial', 'Ballistix BL2K8G30C15U4B 16GB', '', '', NULL, 50, 80, NULL, 4, NULL),
(23, 'RAM', 'Corsair', 'Mac Memory 16GB', '', '', NULL, 22, 85, NULL, 3, NULL),
(24, 'RAM', 'Corsair', 'Vengeance CMSX16GX4M2A2666C18 16GB', '', '', NULL, 30, 65, NULL, 4, NULL),
(25, 'RAM', 'GSkill', 'Trident Z RGB 16GB DDR4', '', '', NULL, 5, 310, NULL, 4, NULL),
(26, 'MOTHERBOARD', 'Asus', 'TUF X570-PLUS', '', '', NULL, 44, 239, 'AM5', 5, 'ATX'),
(27, 'MOTHERBOARD', 'Asus', 'Prime X570-Pro', '', '', NULL, 44, 285, 'AM4', 4, 'MicriATX'),
(28, 'MOTHERBOARD', 'Asus', 'TUF Z690-PLUS', '', '', NULL, 52, 299, 'LG1200', 5, 'MicriATX'),
(29, 'MOTHERBOARD', 'Msi', 'B450 Gaming Plus Max', '', '', NULL, 12, 219, 'AM4', 4, 'ATX'),
(30, 'MOTHERBOARD', 'Msi', 'Pro B660M-G', '', '', NULL, 31, 229, 'AM5', 5, 'MiniATX'),
(31, 'MOTHERBOARD', 'Msi', 'Mag Z690 Tomahawk', '', '', NULL, 32, 269, 'LGA1700', 5, 'ATX'),
(32, 'MOTHERBOARD', 'Gigabyte', 'X570 AORUS ULTRA', '', '', NULL, 67, 259, 'AM5', 5, 'ATX'),
(33, 'MOTHERBOARD', 'Gigabyte', 'Aorus Elite ', '', '', NULL, 12, 199, 'AM4', 4, 'MicriATX'),
(34, 'MOTHERBOARD', 'Gigabyte', 'Gaming B660M', '', '', NULL, 22, 310, 'LGA1700', 5, 'MicriATX'),
(35, 'MOTHERBOARD', 'Gigabyte', 'Z790 UD', '', '', NULL, 39, 279, 'LGA1700', 5, 'ATX'),
(37, 'CASE', 'Antec', 'DF600 FLUX', '', '', NULL, 12, 92, NULL, NULL, 'MID'),
(38, 'CASE', 'Antec', 'P5 MICRO TOWER', '', '', NULL, 12, 95, NULL, NULL, 'MINI'),
(39, 'CASE', 'Antec', 'DP31', '', '', NULL, 22, 109, NULL, NULL, 'MINI'),
(40, 'CASE', 'Antec', 'NX200M', '', '', NULL, 1, 89, NULL, NULL, 'MINI'),
(41, 'CASE', 'Asus', 'GX601 ROG ', '', '', NULL, 31, 129, NULL, NULL, 'MID'),
(42, 'CASE', 'MSI', 'QUIETUDE 100S', '', '', NULL, 41, 99, NULL, NULL, 'MID'),
(43, 'CASE', 'Cortek', 'GENESI TOWER ATX', '', '', NULL, 5, 107, NULL, NULL, 'MINI'),
(44, 'ARCHIVIAZIONE', 'Western Digital', 'WD GREEN', '1TB\r\n', '', NULL, 31, 40, 'Sata', NULL, ''),
(45, 'ARCHIVIAZIONE', 'Western Digital', 'WD GR,EN', '2TB-HDD\r\n', '', NULL, 1, 79, 'Sata', NULL, ''),
(46, 'ARCHIVIAZIONE', 'Western Digital', 'WD GREEN', '4TB-HDD\r\n', '', NULL, 11, 99, 'Sata', NULL, ''),
(47, 'ARCHIVIAZIONE', 'Seagate', 'Barracuda', '1TB-HDD\r\n', '', NULL, 14, 42, 'Sata', NULL, ''),
(48, 'ARCHIVIAZIONE', 'Seagate', 'Barracuda', '2TB-HDD\r\n', '', NULL, 4, 67, 'Sata', NULL, ''),
(49, 'ARCHIVIAZIONE', 'Seagate', 'Barracuda', '4TB-HDD\r\n', '', NULL, 7, 95, 'Sata', NULL, ''),
(50, 'ARCHIVIAZIONE', 'Toshiba', 'MK3000', '1TB-HDD\r\n', '', NULL, 21, 39, 'Sata', NULL, ''),
(51, 'ARCHIVIAZIONE', 'Toshiba', 'MK3000', '2TB-HDD\r\n', '', NULL, 9, 62, 'Sata', NULL, ''),
(52, 'ARCHIVIAZIONE', 'Toshiba', 'MK3000', '4TB-HDD\r\n', '', NULL, 10, 89, 'Sata', NULL, ''),
(53, 'ARCHIVIAZIONE', 'Hitachi', 'Ultra Star', '1TB-HDD\r\n', '', NULL, 1, 38, 'Sata', NULL, ''),
(54, 'ARCHIVIAZIONE', 'Hitachi', 'Ultra Star', '2TB-HDD\r\n', '', NULL, 10, 67, 'Sata', NULL, ''),
(55, 'ARCHIVIAZIONE', 'Hitachi', 'Ultra Star', '4TB-HDD\r\n', '', NULL, 2, 94, 'Sata', NULL, ''),
(56, 'ARCHIVIAZIONE', 'Sonnics', 'GK200', '1TB-HDD\r\n', '', NULL, 44, 45, 'Sata', NULL, ''),
(57, 'ARCHIVIAZIONE', 'Sonnics', 'GK200', '2TB-HDD\r\n', '', NULL, 5, 67, 'Sata', NULL, ''),
(58, 'ARCHIVIAZIONE', 'Sonnics', 'GK200', '4TB-HDD\r\n', '', NULL, 12, 93, 'Sata', NULL, ''),
(59, 'ARCHIVIAZIONE', 'Western Digital', 'WD Blue', '250GB-SSD\r\n', '', NULL, 31, 33, 'Sata', NULL, ''),
(60, 'ARCHIVIAZIONE', 'Western Digital', 'WD Blue', '500GB-SSD\r\n', '', NULL, 6, 56, 'Sata', NULL, ''),
(61, 'ARCHIVIAZIONE', 'Western Digital', 'WD Blue', '1TB-SSD\r\n', '', NULL, 5, 99, 'Sata', NULL, ''),
(62, 'ARCHIVIAZIONE', 'Western Digital', 'WD Blue', '2TB-SSD\r\n', '', NULL, 10, 189, 'Sata', NULL, ''),
(63, 'ARCHIVIAZIONE', 'Western Digital', 'WD Blue', '4TB-SSD\r\n', '', NULL, 2, 370, 'Sata', NULL, ''),
(64, 'ARCHIVIAZIONE', 'Samsung', 'EVO 870', '250GB-SSD\r\n', '', NULL, 22, 45, 'Sata', NULL, ''),
(65, 'ARCHIVIAZIONE', 'Samsung', 'EVO 870', '500GB-SSD\r\n', '', NULL, 1, 69, 'Sata', NULL, ''),
(66, 'ARCHIVIAZIONE', 'Samsung', 'EVO 870', '1TB-SSD\r\n', '', NULL, 41, 99, 'Sata', NULL, ''),
(67, 'ARCHIVIAZIONE', 'Samsungl', 'EVO 870', '2TB-SSD\r\n', '', NULL, 21, 199, 'Sata', NULL, ''),
(68, 'ARCHIVIAZIONE', 'Samsung', 'EVO 870', '4TB-SSD\r\n', '', NULL, 4, 349, 'Sata', NULL, ''),
(69, 'ARCHIVIAZIONE', 'Silicon Power', 'SP A55', '250GB-SSD\r\n', '', NULL, 7, 25, 'Sata', NULL, ''),
(70, 'ARCHIVIAZIONE', 'Silicon Power', 'SP A55', '500GB-SSD\r\n', '', NULL, 8, 49, 'Sata', NULL, ''),
(71, 'ARCHIVIAZIONE', 'Silicon Power', 'SP A55', '1TB-SSD\r\n', '', NULL, 9, 79, 'Sata', NULL, ''),
(72, 'ARCHIVIAZIONE', 'Silicon Power', 'SP A55', '2TB-SSD\r\n', '', NULL, 51, 129, 'Sata', NULL, ''),
(73, 'ARCHIVIAZIONE', 'Silicon Power', 'SP A55', '4TB-SSD\r\n', '', NULL, 11, 199, 'Sata', NULL, ''),
(74, 'ARCHIVIAZIONE', 'Crucial', 'MX 500', '250GB-SSD\r\n', '', NULL, 21, 40, 'Sata', NULL, ''),
(75, 'ARCHIVIAZIONE', 'Crucial', 'MX 500', '500GB-SSD\r\n', '', NULL, 4, 67, 'Sata', NULL, ''),
(76, 'ARCHIVIAZIONE', 'Crucial', 'MX 500', '1TB-SSD\r\n', '', NULL, 5, 95, 'Sata', NULL, ''),
(77, 'ARCHIVIAZIONE', 'Crucial', 'MX 500', '2TB-SSD\r\n', '', NULL, 8, 179, 'Sata', NULL, ''),
(78, 'ARCHIVIAZIONE', 'Crucial', 'MX 500', '4TB-SSD\r\n', '', NULL, 15, 255, 'Sata', NULL, ''),
(79, 'ARCHIVIAZIONE', 'Kingston', 'AS 400', '250GB-SSD\r\n', '', NULL, 18, 29, 'Sata', NULL, ''),
(80, 'ARCHIVIAZIONE', 'Kingston', 'AS 400', '500GB-SSD\r\n', '', NULL, 22, 45, 'Sata', NULL, ''),
(81, 'ARCHIVIAZIONE', 'Kingston', 'AS 400', '1TB-SSD\r\n', '', NULL, 21, 79, 'Sata', NULL, ''),
(82, 'ARCHIVIAZIONE', 'Kingston', 'AS 400', '2TB-SSD\r\n', '', NULL, 21, 129, 'Sata', NULL, ''),
(83, 'ARCHIVIAZIONE', 'Kingston', 'AS 400', '4TB-SSD\r\n', '', NULL, 6, 209, 'Sata', NULL, ''),
(84, 'ARCHIVIAZIONE', 'Lexar', 'NS 100', '250GB-SSD\r\n', '', NULL, 3, 32, 'Sata', NULL, ''),
(85, 'ARCHIVIAZIONE', 'Lexar', 'NS 100', '500GB-SSD\r\n', '', NULL, 3, 47, 'Sata', NULL, ''),
(86, 'ARCHIVIAZIONE', 'Lexar', 'NS 100', '1TB-SSD\r\n', '', NULL, 4, 89, 'Sata', NULL, ''),
(87, 'ARCHIVIAZIONE', 'Lexar', 'NS 100', '2TB-SSD\r\n', '', NULL, 11, 179, 'Sata', NULL, ''),
(88, 'ARCHIVIAZIONE', 'Lexar', 'NS 100', '4TB-SSD\r\n', '', NULL, 21, 289, 'Sata', NULL, ''),
(89, 'ARCHIVIAZIONE', 'SanDisk', 'SD PLUS', '250GB-SSD\r\n', '', NULL, 20, 34, 'Sata', NULL, ''),
(90, 'ARCHIVIAZIONE', 'SanDisk', 'SD PLUS', '500GB-SSD\r\n', '', NULL, 4, 48, 'Sata', NULL, ''),
(91, 'ARCHIVIAZIONE', 'SanDisk', 'SD PLUS', '1TB-SSD\r\n', '', NULL, 1, 96, 'Sata', NULL, ''),
(92, 'ARCHIVIAZIONE', 'SanDisk', 'SD PLUS', '2TB-SSD\r\n', '', NULL, 8, 199, 'Sata', NULL, ''),
(93, 'ARCHIVIAZIONE', 'SanDisk', 'SD PLUS', '4TB-SSD\r\n', '', NULL, 9, 309, 'Sata', NULL, ''),
(94, 'ARCHIVIAZIONE', 'Samsung', '970 EVO PLUS', '250GB-M.2\r\n', '', NULL, 23, 60, 'PCIEx4', NULL, ''),
(95, 'ARCHIVIAZIONE', 'Samsung', '970 EVO PLUS', '500GB-M.2\r\n', '', NULL, 55, 74, 'PCIEx4', NULL, ''),
(96, 'ARCHIVIAZIONE', 'Samsung', '970 EVO PLUS', '1TB-M.2\r\n', '', NULL, 2, 99, 'PCIEx4', NULL, ''),
(97, 'ARCHIVIAZIONE', 'Samsung', '970 EVO PLUS', '2TB-M.2\r\n', '', NULL, 3, 210, 'PCIEx4', NULL, ''),
(98, 'ARCHIVIAZIONE', 'Western Digital', '970 EVO PLUS', '1TB-M.2\r\n', '', NULL, 30, 0, 'PCIEx4', NULL, ''),
(99, 'ARCHIVIAZIONE', 'Crucial', '970 EVO PLUS', '1TB-M.2\r\n', '', NULL, 31, 0, 'PCIEx4', NULL, ''),
(100, 'ARCHIVIAZIONE', 'Sabrent', '970 EVO PLUS', '1TB-M.2\r\n', '', NULL, 19, 0, 'PCIEx4', NULL, ''),
(101, 'ARCHIVIAZIONE', 'Silicon Power', '970 EVO PLUS', '1TB-M.2\r\n', '', NULL, 13, 0, 'PCIEx4', NULL, ''),
(102, 'ARCHIVIAZIONE', 'Lexar', '970 EVO PLUS', '1TB-M.2\r\n', '', NULL, 15, 0, 'PCIEx4', NULL, ''),
(103, 'ARCHIVIAZIONE', 'Patriot', 'P300', '1TB-M.2\r\n', '', NULL, 12, 0, 'PCIEx4', NULL, ''),
(104, 'ARCHIVIAZIONE', 'Corsair', 'MP600 Pro XT', '1TB-M.2\r\n', '', NULL, 65, 0, 'PCIEx4', NULL, ''),
(105, 'ARCHIVIAZIONE', 'Kingston', 'KC3000', '1TB-M.2\r\n', '', NULL, 7, 0, 'PCIEx4', NULL, ''),
(106, 'WEBCAM', 'Jabra', 'PanaCast', '', '', NULL, 1, 370, NULL, NULL, NULL),
(108, 'WEBCAM', 'Logitech', 'BCC950', '', '', NULL, 10, 177, NULL, NULL, NULL),
(109, 'WEBCAM', 'Logitech', 'Brio', '', '', NULL, 4, 170, NULL, NULL, NULL),
(110, 'WEBCAM', 'Anker', 'PowerConf C300', '', '', NULL, 6, 110, NULL, NULL, NULL),
(111, 'WEBCAM', 'Razer', 'Kiyo', '', '', NULL, 22, 71, NULL, NULL, NULL),
(112, 'WEBCAM', 'Logitech', 'C922 Pro', '', '', NULL, 14, 81, NULL, NULL, NULL),
(113, 'WEBCAM', 'Logitech', 'StreamCam', '', '', NULL, 31, 112, NULL, NULL, NULL),
(114, 'WEBCAM', 'Logitech', 'PanaCast', '', '', NULL, 13, 80, NULL, NULL, NULL),
(115, 'WEBCAM', 'Logitech', 'C925e', '', '', NULL, 3, 73, NULL, NULL, NULL),
(116, 'WEBCAM', 'Jabra', 'C920 HD Pro', '', '', NULL, 3, 67, NULL, NULL, NULL),
(117, 'WEBCAM', 'Angetube', 'Full HD 1080p Webcam', '', '', NULL, 7, 60, NULL, NULL, NULL),
(118, 'WEBCAM', 'Papalook', 'Webcam HD', '', '', NULL, 6, 42, NULL, NULL, NULL),
(119, 'WEBCAM', 'NexiGo', 'StreamCam N930E', '', '', NULL, 8, 370, NULL, NULL, NULL),
(121, 'ALIMENTATORE', 'Corsair', 'CX450M', '', '', 450, 2, 0, NULL, NULL, NULL),
(122, 'ALIMENTATORE', 'Corsair', 'CX450M', 'SM', '', 450, 2, 0, NULL, NULL, NULL),
(123, 'ALIMENTATORE', 'Corsair', 'CX450M', 'SM', '', 550, 2, 0, NULL, NULL, NULL),
(124, 'ALIMENTATORE', 'Corsair', 'CX450M', 'SM', '', 650, 2, 0, NULL, NULL, NULL),
(125, 'ALIMENTATORE', 'Corsair', 'CX450M', 'SM', '', 750, 2, 0, NULL, NULL, NULL),
(126, 'ALIMENTATORE', 'Corsair', 'CX450M', 'SM', '', 850, 2, 0, NULL, NULL, NULL),
(127, 'ALIMENTATORE', 'EVGA', '650 BQ', 'SM', '', 650, 2, 0, NULL, NULL, NULL),
(128, 'ALIMENTATORE', 'Sharkoon', 'WPM Gold Zero ', 'SM', '', 750, 2, 0, NULL, NULL, NULL),
(129, 'ALIMENTATORE', 'Seasonic', 'FOCUS GX-550', 'M', '', 550, 2, 0, NULL, NULL, NULL),
(130, 'ALIMENTATORE', 'Seasonic', 'FOCUS GX-550', 'M', '', 650, 2, 0, NULL, NULL, NULL),
(131, 'ALIMENTATORE', 'Seasonic', 'FOCUS GX-550', 'M', '', 750, 2, 0, NULL, NULL, NULL),
(132, 'ALIMENTATORE', 'Seasonic', 'FOCUS GX-550', 'M', '', 850, 2, 0, NULL, NULL, NULL),
(133, 'ALIMENTATORE', 'Cooler Master', 'MWE 750 Gold V2', 'M', '', 750, 2, 0, NULL, NULL, NULL),
(134, 'ALIMENTATORE', 'Corsair', 'RM850x', '', '', 550, 2, 0, NULL, NULL, NULL),
(135, 'ALIMENTATORE', 'Corsair', 'RM850x', '', '', 650, 2, 0, NULL, NULL, NULL),
(136, 'ALIMENTATORE', 'Corsair', 'RM850x', '', '', 750, 2, 0, NULL, NULL, NULL),
(137, 'ALIMENTATORE', 'Corsair', 'RM850x', '', '', 850, 2, 0, NULL, NULL, NULL),
(138, 'ALIMENTATORE', 'Corsair', 'RM850x', '', '', 1000, 2, 0, NULL, NULL, NULL),
(139, 'DISSIPATORE', 'Cooler Master', 'MasterLiquid Lite 240', '', '', NULL, 21, 52, NULL, NULL, NULL),
(140, 'DISSIPATORE', 'Noctua', 'NH-D15', '', '', NULL, 5, 52, NULL, NULL, NULL),
(141, 'DISSIPATORE', 'Deep Cool', 'Assassin 3', '', '', NULL, 2, 109, NULL, NULL, NULL),
(142, 'DISSIPATORE', 'Cooler Master', 'Master Air AM620M', '', '', 0, 8, 95, NULL, NULL, NULL),
(143, 'DISSIPATORE', 'Arctic Freezer', '34 eSports Duo', '', '', NULL, 13, 66, NULL, NULL, NULL),
(144, 'DISSIPATORE', 'Noctua', 'NH-P1', '', '', NULL, 16, 119, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `Lista_Tipo`
--

CREATE TABLE `Lista_Tipo` (
  `idC` int(11) NOT NULL,
  `Tipo_compatibilita` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dump dei dati per la tabella `Lista_Tipo`
--

INSERT INTO `Lista_Tipo` (`idC`, `Tipo_compatibilita`) VALUES
(1, 'CPU'),
(2, 'RAM'),
(3, 'MOTHERBOARD'),
(4, 'CASE'),
(5, 'ARCHIVIAZIONE'),
(6, 'WEBCAM'),
(7, 'ALIMENTATORE'),
(8, 'DISSIPATORE');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `articolo`
--
ALTER TABLE `articolo`
  ADD PRIMARY KEY (`idA`);

--
-- Indici per le tabelle `Lista_Tipo`
--
ALTER TABLE `Lista_Tipo`
  ADD PRIMARY KEY (`idC`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `articolo`
--
ALTER TABLE `articolo`
  MODIFY `idA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT per la tabella `Lista_Tipo`
--
ALTER TABLE `Lista_Tipo`
  MODIFY `idC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
