-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 05, 2023 alle 15:25
-- Versione del server: 10.4.24-MariaDB
-- Versione PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assemblaggio pc`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `articolo`
--

CREATE TABLE `articolo` (
  `idA` int(11) NOT NULL,
  `Tipo` char(30) NOT NULL,
  `Nome_articolo` varchar(255) NOT NULL,
  `Descrizione` varchar(4095) NOT NULL,
  `Immagine` char(30) NOT NULL,
  `Consumo` int(11) DEFAULT NULL,
  `Disponibilità` int(11) NOT NULL,
  `Prezzo` int(11) NOT NULL,
  `Socket_CPU` char(10) DEFAULT NULL,
  `Ram_DDR` int(2) DEFAULT NULL,
  `Formato_MB` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `idS` int(11) NOT NULL,
  `Username` char(30) NOT NULL,
  `idA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `lista_tipo`
--

CREATE TABLE `lista_tipo` (
  `idC` int(11) NOT NULL,
  `Tipo_Articolo` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `lista_tipo`
--

INSERT INTO `lista_tipo` (`idC`, `Tipo_Articolo`) VALUES
(1, 'CPU Intel'),
(2, 'CPU AMD'),
(3, 'Scheda Madre Intel'),
(4, 'Scheda Madre AMD'),
(5, 'RAM'),
(6, 'SSD'),
(7, 'M.2'),
(8, 'HD meccanico'),
(9, 'Scheda Grafica'),
(10, 'Dissipatore Ventola'),
(11, 'Dissipatore AOL'),
(12, 'Trasformatore'),
(13, 'Case'),
(14, 'Monitor'),
(15, 'Mouse'),
(16, 'Tastiera'),
(17, 'Webcam'),
(18, 'Cuffie Microfono');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `Username` char(30) NOT NULL,
  `Password` char(30) NOT NULL,
  `Cognome` char(30) NOT NULL,
  `Nome` char(30) NOT NULL,
  `Indirizzo` varchar(2047) DEFAULT NULL,
  `Cell` int(20) DEFAULT NULL,
  `Mail` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`Username`, `Password`, `Cognome`, `Nome`, `Indirizzo`, `Cell`, `Mail`) VALUES
('Prova1', 'password', 'Opor', 'Cody', 'Via Bava Beccaris 10', 1234567899, 'bava.beccaris@milano.it');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `articolo`
--
ALTER TABLE `articolo`
  ADD PRIMARY KEY (`idA`);

--
-- Indici per le tabelle `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`idS`),
  ADD KEY `Username` (`Username`),
  ADD KEY `idA` (`idA`);

--
-- Indici per le tabelle `lista_tipo`
--
ALTER TABLE `lista_tipo`
  ADD PRIMARY KEY (`idC`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`Username`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `articolo`
--
ALTER TABLE `articolo`
  MODIFY `idA` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `carrello`
--
ALTER TABLE `carrello`
  MODIFY `idS` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `lista_tipo`
--
ALTER TABLE `lista_tipo`
  MODIFY `idC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `carrello`
--
ALTER TABLE `carrello`
  ADD CONSTRAINT `carrello_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `utente` (`Username`),
  ADD CONSTRAINT `carrello_ibfk_2` FOREIGN KEY (`idA`) REFERENCES `articolo` (`idA`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
