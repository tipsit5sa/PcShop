<?php
class Articolo
  { 
    public $idA;
    public $Tipo;
    public $Descrizione;
    public $Nome_articolo;
    public $Immagine;
    public $Consumo;
    public $Disponibilità;
    public $Prezzo;
    public $Socket_CPU;
    public $Ram_DDR;
    public $Formato_MB;


    function __construct()
    {
    }
    function set_idA($idA)
    {
        $this->idA = $idA;
    }

    function set_Tipo($Tipo)
    {
        $this->Tipo = $Tipo;
    }
    function set_Nome_articolo($Nome_articolo)
    {
        $this->Nome_articolo = $Nome_articolo;
    }
    function set_Descrizione($Descrizione)
    {
        $this->Descrizione = $Descrizione;
    }
    function set_Immagine($Immagine)
    {
        $this->Immagine = $Immagine;
    }
    function set_Consumo($Consumo)
    {
        $this->Consumo = $Consumo;
    }
     function set_Disponibilità($Disponibilità)
    {
        $this->Disponibilità= $Disponibilità;
    }
    function set_Prezzo($Prezzo)
    {
        $this->Prezzo= $Prezzo;
    }
    function set_Socket_CPU($Socket_CPU)
    {
        $this->Socket_CPU= $Socket_CPU;
    }
    function set_Ram_DDR($Socket_Ram_DDR)
    {
        $this->Socket_Ram_DDR= $Ram_DDR;
    }
    function set_Formato_MB($Socket_Formato_MB)
    {
        $this->Socket_Formato_MB= $Formato_MB;
    }

    function get_idA()
    {
        return $this->idA;
    }
   function get_Tipo()
    {
        return $this->Tipo;
    }
    function get_Nome_articolo()
    {
        return $this->Nome_articolo;
    }
    function get_Descrizione()
    {
        return $this->Descrizione;
    }
    function get_Immagine()
    {
        return $this->Immagine;
    }
      function get_Consumo()
    {
        return $this->Consumo;
    }
    function get_Disponibilità()
    {
        return $this->Disponibilità;
    }
    function get_Prezzo()
    {
        return $this->Prezzo;
    }
    function get_Socket_CPU()
    {
        return $this->Socket_CPU;
    }
    function get_Ram_DDR()
    {
        return $this->Ram_DDR;
    }
    function get_Formato_MB()
    {
        return $this->Formato_MB;
    } 
   
  }
 ?>