

function getData(url){
    var xmlhttp = new XMLHttpRequest();
    
    
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            alert ("dati ricevuti");
            myFunction(myArr);
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();

}

function myFunction(arr) {
    var out = "";
    var i;
    for(i = 0; i < arr.length; i++) {
        out += '<a href="' + arr[i].url + '">' +
        arr[i].display + '</a><br>';
    }
    document.getElementById("id01").innerHTML = out;
}


function aggiungiArticolo(idA){
    //costruisco l'oggetto articolo
    var art = {
      item: idA,
      qty: 1
    };

    //trasformo l'oggetto articolo in oggetto json
    var jsonStr = JSON.stringify( art );
    //memorizzo il json in un coockie
    sessionStorage.setItem( "carrello", jsonStr );
    alert("prodotto aggiunto id:" + idA);
    
    //rcupero il json dal coockie
    var carrelloValue = sessionStorage.getItem( "carrello" );
    //trasformo il json in oggetto
    var carrelloObj = JSON.parse( carrelloValue );
    //stampo il valore dell'oggetto sulla console
    console.log(carrelloObj);
   
    
  }